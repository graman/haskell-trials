module Quantifiers where
{-# OPTIONS_GHC -Wall #-}
every ::[a]->(a->Bool)->Bool
some ::[a]->(a->Bool)->Bool
every [] _=error "Domain cannot be empty"
every x p = and (map p x )
some x p= or(map p x)
{--
Define a function unique :: (a -> Bool) -> [a] -> Bool
that gives True for unique p xs just in case there is exactly one object among
xs that satisfies p
--}
unique::(a->Bool)->[a]->Bool
unique _ []=error "domain cannot be empty"
unique a x=if (count a x)==(1::Integer) then True else False
            where count _n []=(0::Integer)
                  count n (m:ms)=if (n m) then ((count n ms) + 1) else (count n ms)
{--
WIP : Define a function parity :: [Bool] -> Bool that gives True
for parity xs just in case an even number of the xss equals True.
--}

parity ::[Bool]->Bool
parity []=False
parity x=even(parity' x (0::Integer))
         where parity' [] a=a
               parity' (l:ls) a=if(l) then (parity' ls (a+1)) else (parity' ls a)
{--WIP
Define a function evenNR :: (a -> Bool) -> [a] -> Bool
that gives True for evenNR p xs just in case an even number of the xss have
property p. (Use the parity function from the previous exercise.)
--}
evenNR :: (a -> Bool) -> [a] -> Bool
evenNR _p []=False
evenNR p x= (parity (map p x))