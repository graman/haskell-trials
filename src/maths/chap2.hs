module Chap2 where
{-# OPTIONS_GHC -Wall #-}
{-Ganesh Raman-}
--all type declarations

formula2 :: Bool -> Bool -> Bool
func1 :: [Bool -> Bool]
ttable :: ([Bool], [Bool])
law_of_double_negation :: Bool
law_of_idempotence :: Bool
law_of_contraposition_1 :: Bool
law_of_contraposition_2 :: Bool
law_of_contraposition_3 :: Bool
law_of_contraposition_4 :: Bool
law_of_contraposition_5 :: Bool
law_of_contraposition_6 :: Bool
law_of_contraposition_7 :: Bool
law_of_commutativity_1 :: Bool
law_of_commutativity_2 :: Bool
de_morgans_law_1 :: Bool
de_morgans_law_2 :: Bool
associative_laws :: Bool
distributive_laws :: Bool
ex2_20_1 :: Bool

--all implementations follow

infix 1 ==>
(==>) :: Bool -> Bool -> Bool
x ==> y = (not x) || y
infix 1 <=>
(<=>) :: Bool -> Bool -> Bool
x <=> y = x == y
infix 2 <+>
(<+>) :: Bool->Bool->Bool
(<+>) a b=(a/=b)


formula2 p q = ((not p) && (p ==> q) <=> not (q && (not p)))
--example for TT of ||
func1 = map (||) [True,False]
ttable =(map (func1!!0) [True,False],map (func1!!1) [True,False])
valid1 :: (Bool -> Bool) -> Bool
valid1 bf = (bf True) && (bf False)
valid2 :: (Bool -> Bool -> Bool) -> Bool
valid2 bf = (bf True True) && (bf True False) && (bf False True) && (bf False False)
valid3 :: (Bool -> Bool -> Bool -> Bool) -> Bool
valid3 bf = and [ bf p q r | p <- [True,False],q <- [True,False], r <- [True,False]]
valid4 :: (Bool -> Bool -> Bool -> Bool -> Bool) -> Bool
valid4 bf = and [ bf p q r s | p <- [True,False], q <- [True,False], r <- [True,False], s <- [True,False]]
logEquiv1 :: (Bool -> Bool) -> (Bool -> Bool) -> Bool
logEquiv1 bf1 bf2 =(bf1 True <=> bf2 True) && (bf1 False <=> bf2 False)
logEquiv2 :: (Bool->Bool->Bool)->(Bool->Bool->Bool)->Bool
logEquiv2 bf1 bf2 =and [(bf1 p q) <=> (bf2 p q) | p <- [True,False], q <- [True,False]]
logEquiv3 :: (Bool->Bool->Bool->Bool)->(Bool->Bool->Bool->Bool)->Bool
logEquiv3 bf1 bf2 =and [(bf1 p q r) <=> (bf2 p q r)|p<-[True,False],q<-[True,False],r<-[True,False]]
law_of_double_negation=((logEquiv1 id (\p-> (not(not p))))==True)
law_of_idempotence=(logEquiv1 id (\p->(p && p))) && (logEquiv1 id (\p->p || p))
law_of_contraposition_1=(logEquiv2 (\p q->p==>q) (\p q->((not p) || q) ))
-- !(P => Q) == P and !Q
law_of_contraposition_2=(logEquiv2 (\p q-> (not (p==>q)) )  (\p q -> (p && (not q))))
-- !q=>!p == p=>q
law_of_contraposition_3=(logEquiv2 (\p q -> ((not q)==>(not p))) (\p q ->(p==>q)))
-- p=>!q == q=>!p
law_of_contraposition_4=(logEquiv2 (\p q-> (p==>(not q)))(\p q->(q==>(not p))))
-- !p=>q == !q=>p
law_of_contraposition_5=(logEquiv2 (\p q-> (not p)==>q)(\p q-> (not q)==>p))
--  p<==>q == p=>q and q=>p
law_of_contraposition_6=(logEquiv2 (\p q->p<=>q) (\p q-> ((p==>q)&&(q==>p))))
--  p<==>q == p and q or !p and !q
law_of_contraposition_7=(logEquiv2 (\p q-> p<=>q) (\p q->((p && q)||( (not p) && (not q)))))
-- p and q= q and p
law_of_commutativity_1 =(logEquiv2 (\p q->p&&q)(\p q-> q&&p))
-- p or q = q or p
law_of_commutativity_2 =(logEquiv2 (\p q->p||q)(\p q-> q||p))
-- not(p and q)= (!p or !q) 
de_morgans_law_1=(logEquiv2(\p q-> (not (p &&q)))(\p q->((not p)||(not q))))
de_morgans_law_2=(logEquiv2(\p q-> (not (p ||q)))(\p q->((not p)&&(not q))))
-- associative law p and (q and r) = (p and q) and r :: p and (q and r) = (p and q) and r 
associative_laws= and [ logEquiv3(\p q r-> p `x` (q `x` r)) (\p q r -> ((p `x` q)`x` r)) |x<-[(&&),(||)]]
--dsistributive law p and (q or r)== (p and q) or (p and r) ;;  p or (q and r) == (p or q) and (p or r)
distributive_laws= and [logEquiv3 (\p q r -> p `x` (q `y` r) )(\p q r -> (p `x` q) `y` (p `x` r))| x<-[(&&)],y<-[(||)]]
ex2_20_1=(logEquiv2 (\p q-> (not p)==>q) (\p q->p==>(not q)))