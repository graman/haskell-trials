module Proofs_chap3 where
{-# OPTIONS_GHC -Wall #-}
infix 1 ==>
(==>) :: Bool -> Bool -> Bool
x ==> y = (not x) || y
infix 1 <=>
(<=>) :: Bool -> Bool -> Bool
x <=> y = x == y
infix 2 <+>
(<+>) :: Bool->Bool->Bool
(<+>) a b=(a/=b)
--ex 3.7 if p => q then !q=>!p
{--
Logical proof:
Given p=>q
thus taking negation of negation given
not(not(p=>q))
not(p ^ not q)
not(not q ^ p)
which is as good as ^q=>^p 
 
--}
ex3_7::Bool
ex3_7= lhs==rhs
       where lhs=[if(not q) then (not p) else True|p<-[True,False],q<-[True,False] ]
             rhs=[if(p) then q else True|p<-[True,False],q<-[True,False]]
{--
ex 3.7 b 
Given P<=>Q then prove ^P<=>^Q
--}
ex_3_7_b :: Bool

ex_3_7_b=lhs==rhs
         where lhs=[(if p then q else True) && (if q then p else True) |p<-[True,False],q<-[True,False]]
               rhs=[(if (not p) then (not q) else True) && (if (not q) then (not p) else True) |p<-[True,False],q<-[True,False]]
{--
(p =>q) => p for all p and q
--}
ex_3_9 :: Bool
ex_3_9= and lhs
       where lhs=[(if (p_q p q) then p else True) == p|p<-[True,False],q<-[True,False]]
             p_q=(\p q -> if p then q else True)
{--
from p or q , neg p  it follows that q
--}
ex_3_9_1::Bool
ex_3_9_1=and lhs
         where lhs=[(p||q)==q|p<-[False],q<-[True,False]]
{--
Deduction rule example : P=>Q , Q=>R then prove that P=>R
--}
deduction_rule_example_proof :: Bool
deduction_rule_example_proof=and rhs
                             where 
                                   rhs=[ (((p==>q) && (q==>r))==>(p==>r))|p<-[True,False],q<-[True,False],r<-[True,False]]
{--
Sieve of eratosthenes
--}
sieve::[Integer]->[Integer]
sieve []=[]
sieve (0:xn)=sieve xn
sieve (x:xn)=x:sieve (mark xn 1 x)
             where mark :: [Integer]->Integer->Integer->[Integer]
                   mark [] _a _b  =[]
                   mark (y:yn) k m| k==m= 0:(mark yn 1 m)
                                  | otherwise =y:(mark yn (k+1) m)
{--
odd number generator
--}
oddsFrom3 :: [Integer]
oddsFrom3 = 3 : map (+2) oddsFrom3
{--
Sundaram sieve :
If N is an element of oddsFrom3+1 then 2N+1 is not prime , if N does not occur , then 2N+1 is a prime
--}
sundaram_sieve::[Integer]
sundaram_sieve=sieve (2:oddsFrom3)
{--
Write a Haskell program to refute the following statement about
prime numbers: if p1; : : : ; pk are all the primes < n, then (p1..pk) + 1 is a
prime
--}
primes1 :: [Integer]
primes1 = 2 : filter prime [3..]
prime :: Integer -> Bool
prime n | n < 1 = error "not a positive integer"
        | n == 1 = False
        | otherwise = ldp n == n
ldp :: Integer -> Integer
ldp n = ldpf primes1 n
ldpf :: [Integer] -> Integer -> Integer
ldpf [] _n=error"Please do not put null lists!"
ldpf (p:ps) n | rem n p == 0 = p
              | p^(2::Integer) > n = n
              | otherwise = ldpf ps n
refuter::Bool
refuter =  refuter_1 1       
              where refuter_1 k|(  prime ( ( multiplicant (take k primes1) )+1) )==False=False
                               | otherwise =refuter_1 (k+1)
                                where multiplicant []=1
                                      multiplicant (x:xs) = x* multiplicant xs
mersenne :: [(Integer, Integer)]
mersenne = [ (p,2^p - 1) | p <- primes1, prime (2^p - 1) ]
notmersenne :: [(Integer, Integer)]
notmersenne = [ (p,2^p - 1) | p <- primes1, not (prime (2^p-1)) ]
pdivisors :: Integer -> [Integer]
pdivisors n = [ d | d <- [1..(n-1)], rem n d == 0 ]
primePairs :: [(Integer,Integer)]
primePairs =pairs primes1
            where pairs []=[]
                  pairs [_]=[]
                  pairs (x:y:xs)|x+2==y=(x,y):pairs(y:xs)
                                |otherwise =pairs(y:xs)