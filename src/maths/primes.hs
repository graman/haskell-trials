module Primes where
{-# OPTIONS_GHC -Wall #-}
{-Ganesh Raman-}
import Debug.Trace
debug :: c -> String -> c
debug = flip trace
divides :: Integer->(Integer->Bool)
--divides does d divide n? : check if d==0 , if so report an error
divides d n| (d/=0) = ((rem n d) ==0)
		   | otherwise =error "divide by zero"	
--ldf=lowest divisor starting from a threshold 		   
ldf :: Integer->(Integer->Integer)
{- 
	Based on the fact that , if p is the lowest factor of a given number 'n' , then square of p is < n.
	Because :if p*a=n , then a>p => a*p>p*p =>n>p^2
-}
ldf k n | divides k n = k
		| (k*k)>n=n
		| otherwise = ldf (k+1) n
--generic version of a lowest divisior , is just the one that starts with 2 being its lowest threshold
ld :: Integer->Integer		
ld n = ldf 2 n
{- 
	Primes is any positive number greater than 1, whose lowest divisor is none-other than itself!
-}
prime0 :: Integer->Bool
prime0 n|n<1= error "Input should be a positive natural number" --positive
		|n==1 = False --greater than 1
		|otherwise =(ld n)==n --whose lowest divisor is itself!
{-
	of x and y , which is lesser?
-}		
min'::Int->Int->Int
min' x y | x<=y =x
		|otherwise =y
--minimum of a list of Ints		
mntInt' :: [Int]->Int
mntInt' []= error "Empty list!"		
mntInt' [x]= x
mntInt' (x:xs)=min' x (mntInt' xs)
--this below function adds non-same elements from first list to second one  
{-
Define a function removeFst that removes the first occurrence of
an integerm from a list of integers. If m does not occur in the list, the list remains
unchanged.
-}
fstOc :: [Int]->[Int]->Int->[Int]
fstOc [] _z _y = _z
fstOc (x:xs) z n|n==x=(z++xs) 
			    |length(x:xs)>0 = fstOc xs (z++(x:[])) n	
				|otherwise = z
firstOccurence :: [Int]->Int->[Int]
firstOccurence [] _x = error "Empty list should not be provided for fo"				  
firstOccurence z x = fstOc z []  x
--Validating Credit Card Numbers
--step 1 double every second digit starting from behind
--exposed function is evenDoubler of a list
evenDoubler :: [Integer]->[Integer]
evenDoubler  inp=evenDoubler' inp [] (1::Int)
                         where 
                               evenDoubler' [] b _d =b
                               evenDoubler' (x:xs) b d|even d=evenDoubler' xs atEvenSoDoubleAndAppendToResult (next)
                                                      |otherwise=evenDoubler' xs atOddSoAppendToResult (next)
                                                      where twice=2*x
                                                            next=d+1
                                                            append=(++) b
                                                            atEvenSoDoubleAndAppendToResult=(append((twice):[]))
                                                            atOddSoAppendToResult=(append((x):[]))

evenDoublerFromLast :: [Integer]->[Integer]
evenDoublerFromLast x =reverse(evenDoubler (reverse x))

--the real sum of digits function
digitSum :: Integer->Integer
digitSum inp=digitSum' inp 0
                  where 
                        digitSum' x y|(x `mod` 10==0)=x+y
                                     |otherwise = digitSum'  (div x  10) ((x `mod` 10)+y)


--now for finding all sum of all digits across a list
--the formal version only takes a list
sumOfDigitsOfEachItem :: [Integer]->Integer
sumOfDigitsOfEachItem inp=sumOfDigitsOfEachItem' inp 0
                                              where sumOfDigitsOfEachItem' [] y=y
                                                    sumOfDigitsOfEachItem' (x:xs) y= sumOfDigitsOfEachItem' xs (y+(digitSum x))
--reminder when sum is divided by 10 should be 0
isValidCardFromSum' :: Integer ->Bool
isValidCardFromSum' x|(x<=10)=True
					 |otherwise=False
isValidCard :: [Integer]->Bool
isValidCard []=error "Credit/Debit card number cannot be null"
isValidCard x=isValidCardFromSum'(sumOfDigitsOfEachItem( evenDoublerFromLast(x)))
m1 :: Maybe Int
m1=Just 2
type Peg=String
type Move=(Peg,Peg)
--tower of hanoii problem
hanoii::Integer->Peg->Peg->Peg->[Move]
hanoii':: Integer->Peg->Peg->Peg->[Move]->[Move]
hanoii' 0 _a _b _c x=x
hanoii' 1 a b c x= hanoii' 0 a b c (x++((a,b):[]))
hanoii' n a b c x= (hanoii' (n-1) a b c (x++((a,c):[])))++ ((c,b):[])
hanoii	n a b c =hanoii' n a b c []
--We define a function that sorts a list of integers in order of increasing size, by means of the following algorithm:
--an empty list is already sorted.
--if a list is non-empty, we put its minimum in front of the result of sorting the list that results from removing its minimum.
minInList :: [Int]->Int
minInList' :: [Int]->Int->Int->Int
sortInts :: [Int] -> [Int]
sortInts' :: [Int]->[Int]->[Int]
minInList' [] _x _currMin=error "List cannot be empty"
minInList' x a currMin|a>((length x)-1)=currMin 
					  |currMin>(x!!a) = minInList' x (a+1) (x!!a)
					  |otherwise=minInList' x (a+1) currMin
minInList [] =error "List should not be empty"
minInList x =minInList' x 0 maxBound :: Int
joinTailSecond :: ([a], [a]) -> [a]
joinTailSecond (a,[])=a
joinTailSecond (a,b)=a++(tail b)
indexOfItem :: [Int]->Int->Int
indexOfItem' :: [Int]->Int->Int->Int
indexOfItem' [] _a _b=error "Empty list"
indexOfItem' x a b|a>((length x)-1)=error "Element not found!"
				  |(x!!a)==b=a
				  |otherwise =indexOfItem' x (a+1) b
indexOfItem x a =indexOfItem' x 0 a			  
deleteAtA :: [a] -> Int -> [a]
deleteAtA x a=joinTailSecond(splitAt a x)
sortInts' [] _a =_a
sortInts' x a =(sortInts' (deleteAtA (x) (indexOfItem (x) (minInList(x)))) (a++(minInList(x):[])))
sortInts x =sortInts'  x []

{- a sum equivalent to Main.sum in Prelude-}
sum'::[Int]->Int
sum' [] =0
sum' (x:xs)= x+sum'(xs)
{--average of a list of numbers--}
avg::[Int]->Float
avg []=error "Cannot take an empty list for average!"
avg x =let tot=(fromIntegral  (sum' x)) 
           len=(fromIntegral (length x))
       in (tot/len) ;
{-Count number of occurences of a character in a string-}
count :: Char->[Char]->Int
count' :: Char->[Char]->Int->Int
count' _a [] c =c
count' a x c = let (l:ls)=x 
               in  case() of
			        _ | (a==l) -> (count' a ls (c+1))
			          |otherwise ->(count' a ls c)
count _ []=error "String cannot be empty"
count a x=count' a x 0			   
{-count with a where clause-}
count'' ::Char->[Char]->Int->Int
count'' _a [] c=c
count'' a x c | a==l=(count'' a ls (c+1))
			  | otherwise = (count'' a ls (c))
			  where (l:ls)=x		
{-blowup "bang!" should yield "baannngggg!!!!!"-}
blowup ::String->String
blowup a =blowup' a (1::Int) []
              where 
                    blowup' [] _x y=y
                    blowup' (x:xs) y z= (blowup' (xs) (y+1) (z++(take y (repeat x))))
{-Write a function srtString :: [String] -> [String] that
sorts a list of strings in alphabetical order.
First have a function that gets a minimum string from a list of strings
the recursively apply remove first on the mimimum
-}
minStr :: [[Char]] -> [Char]
minStr inputStr=minStr' inputStr (0::Int) ((maxBound::Char ):[])
                where minStr' input thresh currMin|(0::Int)==(length input)=currMin
                                                  |inp<currMin=minStr' _inpss (thresh) inp
                                                  |otherwise=minStr' _inpss (thresh) currMin
                                                    where (inp:_inpss)=input
                                 
srtString ::[String]->[String]
srtString inputStringList=srtString' inputStringList ([])
                    where srtString' [] residue = residue
                          srtString' a b= srtString' xs (b++((minimumString):[]))
                                  where  minimumString=minStr a
                                         xs=removeFstOccOf a (minimumString)
                                         removeFstOccOf ss s=removeFstOccOf'  ss s []
                                                     where 
                                                           removeFstOccOf' [] _yy zz=zz
                                                           removeFstOccOf' (xx:xss) yy zz|xx==yy=removeFstOccOf' [] yy zz++xss
                                                                                         |otherwise=removeFstOccOf' xss yy zz++(xx:[])
lengths :: [[a]]->[Int]
lengths x = map length x
sumLengths :: [[a]]->Int
sumLengths x= (sum (map length x))
primes1 :: [Integer]
primes1 = 2 : filter prime [3..]
prime :: Integer -> Bool
prime n | n < 1 = error "not a positive integer"
        | n == 1 = False
        | otherwise = ldp n == n
ldp :: Integer -> Integer
ldp n = ldpf primes1 n
ldpf :: [Integer] -> Integer -> Integer
ldpf [] _n=error"Please do not put null lists!"
ldpf (p:ps) n | rem n p == 0 = p
              | p^(2::Integer) > n = n
              | otherwise = ldpf ps n

